package edu.escuelait.tienda.configurations;
import edu.escuelait.tienda.clients.ProductClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.support.WebClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

@Configuration
public class WebConfig {

  @Bean
  WebClient webClient() {
    return WebClient.builder()
            .baseUrl("https://dummyjson.com/")
            .build();
  }

  @Bean
  ProductClient postClient(WebClient webClient) {
    HttpServiceProxyFactory httpServiceProxyFactory =
            HttpServiceProxyFactory.builder(WebClientAdapter.forClient(webClient))
                    .build();
    return httpServiceProxyFactory.createClient(ProductClient.class);
  }
}