package edu.escuelait.tienda.configurations;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:report.properties")
@ConfigurationProperties(prefix = "report")
@Data
@ToString
public class ReportConfiguration {

    private String name;
    private String author;
    private String title;

}
