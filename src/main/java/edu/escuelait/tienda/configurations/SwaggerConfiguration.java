package edu.escuelait.tienda.configurations;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
       info = @Info(
               title = "Tienda Palermo API",
               description = "Tienda Palermo IT Microservicio",
               contact = @Contact(
                       name = "Rafael Benedettelli",
                       url = "http://palermo.edu",
                       email = "rblbene@gmail.com"
               ),
               license = @License(
                       name = "LGPL Licence",
                       url = "https://github.com/thombergs/code-examples/blob/master/LICENSE")),
       servers = @Server(url = "http://localhost:8080/palermo/tienda/api/v1/endpoints")
)
public class SwaggerConfiguration {


}
