package edu.escuelait.tienda.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import edu.escuelait.tienda.validators.Cuit;
import edu.escuelait.tienda.validators.groups.OnCreate;
import edu.escuelait.tienda.validators.groups.OnUpdate;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.*;
import lombok.Data;
import lombok.NonNull;

import java.time.LocalDate;

@Data
@JsonPropertyOrder({"id","lastName","name","edad"})
@Schema(description = "Esta es una Persona...") //Swagger doc.
public class Persona {

    @NonNull
    @Schema(description = "Identificador único de persona.", example = "1") //Swagger Doc
    @NotNull(groups = OnCreate.class)
    private Long id;

    @NonNull //Lombok, y es para hacer el atributo mandatorio en el constructor
    @NotBlank(groups = OnUpdate.class)
    private String name;

    private String lastName;

    @Size(min = 3, max = 6)
    @JsonProperty("address")
    private String domicilio;

    @Min(18)
    @Max(80)
    private int edad;

    @AssertFalse
    //@AssertTrue
    private boolean casado;

    @Email
    @JsonIgnore
    private String mail;

    @Positive
    //@PositiveOrZero
    private int puntuacion;

    @Negative
    //@NegativeOrZero
    private int ranking;

    @Past
    //@PastOrPresent
    private LocalDate fechaNacimiento;

    //Ignora propiedad (aplica input-output)
    @JsonIgnore
    @FutureOrPresent
    //@Future
    private LocalDate registro;

    //Custom validators
    @Cuit
    private String cuit;

}
