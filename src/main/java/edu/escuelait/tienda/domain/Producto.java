package edu.escuelait.tienda.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class Producto {

    @NonNull
    private Long id;

    @NonNull
    @JsonProperty("title")
    private String name;

    private String description;

}