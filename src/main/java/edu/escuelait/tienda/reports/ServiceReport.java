package edu.escuelait.tienda.reports;

import edu.escuelait.tienda.domain.Producto;

import java.util.List;

public interface ServiceReport {
    public String printReport(List<Producto> productoList);
}
