package edu.escuelait.tienda.reports;

import edu.escuelait.tienda.configurations.ReportConfiguration;
import edu.escuelait.tienda.domain.Producto;
import edu.escuelait.tienda.stereotypes.Report;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Report
@RequiredArgsConstructor
public class ServiceReportHtmlImpl implements ServiceReport{

    @NonNull
    ReportConfiguration reportConfiguration;

    @Override
    public String printReport(List<Producto> productoList) {
        String report = reportConfiguration.toString();
        return report;
    }
}
