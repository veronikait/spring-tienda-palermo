package edu.escuelait.tienda;

import edu.escuelait.tienda.domain.Producto;
import edu.escuelait.tienda.repositories.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.List;

@SpringBootApplication
@EnableAsync
@Slf4j
public class TIendaOnlineApplication implements CommandLineRunner {

	@Autowired
	private ProductRepository productRepository;

	public static void main(String[] args) {
		SpringApplication.run(TIendaOnlineApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	/*	log.info("Creando productos");
		productRepository.save(new Producto(4L,"PC nueva"));
		productRepository.save(new Producto(5L,"PC nueva 1"));
		Producto productoDistinto = new Producto(6L, "PC nueva 2");
		productoDistinto.setDescription("Este record es distinto de los demas");
		productRepository.save(productoDistinto);


		List<Producto> productos = productRepository.findAll();
		productos.stream().forEach(producto -> {
			log.info("Producto " + producto.toString());
		});*/

	}

}
