package edu.escuelait.tienda.controllers;

import edu.escuelait.tienda.controllers.hatoeas.PersonasModelAssembler;
import edu.escuelait.tienda.domain.Persona;
import edu.escuelait.tienda.exceptions.PersonaDuplicateException;
import edu.escuelait.tienda.exceptions.PersonaNotFoundException;
import edu.escuelait.tienda.validators.groups.OnCreate;
import edu.escuelait.tienda.validators.groups.OnUpdate;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.hateoas.CollectionModel;
//import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/personas")
@Tag(name = "API personas",
        description = "CRUD de personas de tienda") //Swagger doc
public class PersonaRestController {

    @Autowired
    private PersonasModelAssembler assembler;

    //Almacen para tener un lote de datos prueba
    ArrayList<Persona> personas = new ArrayList<>(
            List.of(new Persona(1L, "Rafael"),
                    new Persona(2L, "Miguel"),
                    new Persona(3L, "Alvaro")));

    @ApiResponse(responseCode = "200", description = "Operación exitosa")
    @ApiResponse(responseCode = "400", description = "Error de petición")
    @ApiResponse(responseCode = "404", description = "Recurso no encontrado")
    @Operation(summary = "Recupera una persona por Id", description = "Recupera una persona dado un id de tipo numérico")
    @GetMapping("/{id}")
    public ResponseEntity<?> getPersonaById(
            @Parameter(description = "Id de persona. Valor entero", required = true, example = "1")
            @PathVariable Long id) {

        //pedido invalido
        if (id < 0) {
            return ResponseEntity.badRequest().build();
        }

        Persona persona = this.personas.stream().filter(per -> id == per.getId()).
                findFirst().orElseThrow(() -> new PersonaNotFoundException("no existe la persona"));

        return ResponseEntity.ok(assembler.toModel(persona));

    }

    @GetMapping("/{id}/productos")
    public ResponseEntity<?> getProductosPorPersona(
            @PathVariable Long id) {
            return null;
    }

    @GetMapping
    public ResponseEntity<?> listPersonas() {

        MDC.put("modeloId","personas");
        log.info("Recuperando personas");

        return ResponseEntity.ok(assembler.toCollectionModel(this.personas));
    }

    @PostMapping
    public ResponseEntity<?> createPersona(@Validated(OnCreate.class) @RequestBody Persona persona) {

        if (personas.stream().filter(p -> p.getId() == persona.getId()).count()>0){
            throw new PersonaDuplicateException("id de persona duplicada");
        }

        this.personas.add(persona);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(persona.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping
    public ResponseEntity<?> updatePersona(@Validated(OnUpdate.class) @RequestBody Persona persona) {

        return this.personas.stream().filter(per -> per.getId() == persona.getId()).
                findFirst().map(per -> {
                    per.setLastName(persona.getLastName());
                    per.setName(persona.getName());
                    //TODO mas atributos
                    return ResponseEntity.ok(per);
                }).orElseThrow(() -> new PersonaNotFoundException("No se encontro la persona"));

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePersona(@PathVariable Long id) {

        return this.personas.stream().filter(persona -> id == persona.getId()).
                findFirst().map(persona -> {
                            this.personas.remove(persona);
                            return ResponseEntity.noContent().build();
                        }
                ).orElseThrow(() -> new PersonaNotFoundException("no existe la persona"));

    }


}
