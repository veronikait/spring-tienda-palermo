package edu.escuelait.tienda.controllers;

import edu.escuelait.tienda.domain.Producto;
import edu.escuelait.tienda.mappers.ProductoMapper;
import edu.escuelait.tienda.services.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("productos")
@Slf4j
public class ProductoRestController {

    //@Autowired
    private ProductService productService;

    private ProductoMapper productoMapper;


    public ProductoRestController(//@Qualifier("DEPOSITO")
                                  ProductService productService,
                                  ProductoMapper productoMapper) {
        log.info("Creando instancia de ProductRestController");
        this.productService = productService;
        this.productoMapper = productoMapper;

    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getProductById(@PathVariable Long id) {
        return ResponseEntity.ok(productService.getProductById(id).orElseThrow());
    }

    @GetMapping
    public ResponseEntity<?> listAll() {

        MDC.put("modeloId","productos");
        log.info("Recuperando productos");
        log.info("Ejecutando en trhead- {}", Thread.currentThread().getName());
        log.trace("traza en controller");
        return ResponseEntity.ok(productService.getAllProducts());
    }
    

}
