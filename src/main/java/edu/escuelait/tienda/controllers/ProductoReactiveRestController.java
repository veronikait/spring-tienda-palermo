package edu.escuelait.tienda.controllers;

import edu.escuelait.tienda.domain.Producto;
import edu.escuelait.tienda.services.ProductServiceReactiveImpl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequestMapping("reactive")
@RestController
public class ProductoReactiveRestController {

    ProductServiceReactiveImpl productServiceReactive;

    public ProductoReactiveRestController(ProductServiceReactiveImpl productServiceReactive){
        this.productServiceReactive = productServiceReactive;
    }

    @GetMapping(value = "/productos/{id}")
    public Mono<ResponseEntity<Producto>> getProductById(@PathVariable Long id){
        Mono<Producto> product = productServiceReactive.getProductById(id);
        return product.map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/productos",produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Producto> getProducts(){
        return productServiceReactive.getProducts();
    }

}
