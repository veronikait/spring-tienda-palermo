package edu.escuelait.tienda.controllers.hatoeas;

//import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import edu.escuelait.tienda.controllers.PersonaRestController;
import edu.escuelait.tienda.domain.Persona;
//import org.springframework.hateoas.EntityModel;
//import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PersonasModelAssembler  // implements RepresentationModelAssembler<Persona, EntityModel<Persona>>
{

/*  @Override
  public EntityModel<Persona> toModel(Persona persona) {

    return EntityModel.of(persona, //Links
        linkTo(methodOn(PersonaRestController.class).getPersonaById(persona.getId())).withSelfRel(),
        linkTo(methodOn(PersonaRestController.class).listPersonas()).withRel("personas"),
            linkTo(methodOn(PersonaRestController.class).getProductosPorPersona(persona.getId())).withRel("productos")
    );
  }*/

    public Persona toModel(Persona persona) {

        return persona;
    }

    public List<Persona> toCollectionModel(List<Persona> personasList){
        return personasList;
    }
}