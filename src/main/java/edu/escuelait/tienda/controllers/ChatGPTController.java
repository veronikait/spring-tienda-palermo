package edu.escuelait.tienda.controllers;

import io.github.flashvayne.chatgpt.service.ChatgptService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/chat-gpt")
@RequiredArgsConstructor
public class ChatGPTController {

    @NonNull
    private ChatgptService chatgptService;

    @GetMapping("chat/{message}")
    public ResponseEntity<?> chat(@PathVariable String message){
        String responseMessage = chatgptService.sendMessage(message);
        return ResponseEntity.ok(responseMessage);
    }

}
