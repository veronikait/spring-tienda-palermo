package edu.escuelait.tienda.exceptions.handler;

import edu.escuelait.tienda.exceptions.PersonaDuplicateException;
import edu.escuelait.tienda.exceptions.PersonaNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.time.Instant;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

   @ExceptionHandler(PersonaNotFoundException.class)
   ProblemDetail handleBookmarkNotFoundException(PersonaNotFoundException e) {

        return buildProblemDetail(e,"No se encuentra la persona",HttpStatus.NOT_FOUND);
   }

    @ExceptionHandler(PersonaDuplicateException.class)
    ProblemDetail handleBookmarkNotFoundException(PersonaDuplicateException e) {

        return buildProblemDetail(e,"Ya se encuentra una persona con el id indicado",HttpStatus.CONFLICT);
    }

    public ProblemDetail buildProblemDetail(Exception e,String title,HttpStatus status){

        URI currentUri = ServletUriComponentsBuilder
                .fromCurrentRequest().build().toUri();

        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(status, e.getMessage());
        problemDetail.setTitle(title);
        problemDetail.setType(URI.create("http://localhost:8080/palermo/tienda/api/v1/endpoints/problems"));
        problemDetail.setProperty("errorCategory", "Generic");
        problemDetail.setProperty("timestamp", Instant.now());
        problemDetail.setInstance(currentUri);
        return problemDetail;

    }


}

