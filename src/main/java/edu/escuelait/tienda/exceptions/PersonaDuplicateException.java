package edu.escuelait.tienda.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class PersonaDuplicateException extends RuntimeException{

    public PersonaDuplicateException(String message){
        super(message);
    }

}
