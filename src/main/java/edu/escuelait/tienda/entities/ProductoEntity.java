package edu.escuelait.tienda.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class ProductoEntity {

    @NonNull
    @MongoId
    private Long id;

    @NonNull
    private String name;

    private String description;

}