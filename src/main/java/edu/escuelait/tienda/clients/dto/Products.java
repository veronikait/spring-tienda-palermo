package edu.escuelait.tienda.clients.dto;

import edu.escuelait.tienda.domain.Producto;
import lombok.Data;

import java.util.List;

@Data
public class Products {
    List<Producto> products;
}
