package edu.escuelait.tienda.clients;

import edu.escuelait.tienda.domain.Producto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.Arrays;

@Slf4j
@RestController
@RequestMapping("reactclient/productos")
public class ProductReactClient {




    @GetMapping
    public Flux<Producto> testProductosReactive(){
        WebClient webClient = WebClient.builder()
                .baseUrl("http://localhost:8080/palermo/tienda/api/v1/endpoints/reactive/productos")
                .build();

         webClient.get()
                .retrieve()
                .bodyToFlux(Producto.class)
                .flatMapIterable(Arrays::asList)
                .subscribe(System.out::println);

        return null;
     }



}
