package edu.escuelait.tienda.clients;

import edu.escuelait.tienda.clients.dto.Products;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;


@HttpExchange
public interface ProductClient {

    @GetExchange("products")
    public Products getProductsFromDeposit();

}
