package edu.escuelait.tienda.services;

import edu.escuelait.tienda.clients.ProductClient;
import edu.escuelait.tienda.clients.dto.Products;
import edu.escuelait.tienda.domain.Producto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;


@Service("DEPOSITO")
@ConditionalOnProperty(prefix = "productos", name = "origen", havingValue = "DEPOSITO")
@Slf4j
public class ProductServiceDepositoImpl implements ProductService {

    @Autowired
    ProductClient productClient;

    public ProductServiceDepositoImpl() {

        log.info("Creando instancia de productServiceDepoImpl");
        log.trace("traza de prueba");
        log.debug("traza de debug");
    }


    public Optional<Producto> getProductById(Long id) {
        List<Producto> productsFromDeposit = this.productClient.getProductsFromDeposit().getProducts();
        return productsFromDeposit.stream().filter(p -> p.getId() == id).findFirst();
    }

    public List<Producto> getAllProducts() {
        Products productsFromDeposit = this.productClient.getProductsFromDeposit();
        return productsFromDeposit.getProducts();

    }


}
