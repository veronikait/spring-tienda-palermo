package edu.escuelait.tienda.services;

import edu.escuelait.tienda.domain.Producto;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    public Optional<Producto> getProductById(Long id);

    public List<Producto> getAllProducts();
}
