package edu.escuelait.tienda.services;

import edu.escuelait.tienda.domain.Producto;
import edu.escuelait.tienda.repositories.ProductReactiveRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class ProductServiceReactiveImpl  {

    ProductReactiveRepository productReactiveRepository;

    public ProductServiceReactiveImpl(ProductReactiveRepository productReactiveRepository) {
        this.productReactiveRepository = productReactiveRepository;
    }


    public Mono<Producto> getProductById(Long id){
        Mono<Producto> monoProducto = productReactiveRepository.findById(id);


        return monoProducto;

    }

    public Flux<Producto> getProducts() {
        return productReactiveRepository.findAll();
    }
}
