package edu.escuelait.tienda.services;

import edu.escuelait.tienda.domain.Producto;
import edu.escuelait.tienda.notifications.EmailService;
import edu.escuelait.tienda.reports.ServiceReport;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("LOCAL")
@Slf4j
@ConditionalOnProperty(prefix = "productos", name = "origen", havingValue = "LOCAL")
public class ProductServiceImpl implements ProductService {

    private EmailService emailService;

    public ProductServiceImpl(ServiceReport serviceReport, EmailService emailService) {
        this.emailService = emailService;
        log.info("Creando instancia de productServiceImpl");
        log.info("Generate report {}", serviceReport.printReport(getAllProducts()));

    }

    List<Producto> productos = new ArrayList<>(List.of(
            new Producto(1L, "PC"),
            new Producto(2L, "MOUSE"),
            new Producto(3L, "MONITOR")
    ));

    public Optional<Producto> getProductById(Long id) {
        return productos.stream().filter(p -> p.getId() == id).findFirst();
    }

    public List<Producto> getAllProducts() {
        this.emailService.sendEmail(productos.toString(),"rblbene@gmail.com","Listado de productos");
        return productos;
    }


}
