package edu.escuelait.tienda.repositories;

import edu.escuelait.tienda.domain.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends MongoRepository<Producto,Long> {

}
