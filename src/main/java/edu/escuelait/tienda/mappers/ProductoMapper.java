package edu.escuelait.tienda.mappers;

import edu.escuelait.tienda.domain.Producto;
import edu.escuelait.tienda.entities.ProductoEntity;
import org.mapstruct.Mapper;

@Mapper
public interface ProductoMapper {

    public Producto map(ProductoEntity productoEntity);

}
